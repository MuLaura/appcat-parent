package fr.eql.ai110.appcat.ibusiness;

import fr.eql.ai110.appcat.entity.Owner;

public interface AccountIBusiness {
	
	Owner connect(String login, String password);
	
//	void register(Owner owner);
	
	Owner addOwner(Owner owner, String password);
	

}
