package fr.eql.ai110.appcat.ibusiness;

import java.util.Set;

import fr.eql.ai110.appcat.entity.Cat;
import fr.eql.ai110.appcat.entity.Owner;

public interface CatIBusiness {

	Set<Cat> getCatsByOwner(Owner owner);
	
}
