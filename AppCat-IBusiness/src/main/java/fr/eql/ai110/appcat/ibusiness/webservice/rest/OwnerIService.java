package fr.eql.ai110.appcat.ibusiness.webservice.rest;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import fr.eql.ai110.appcat.entity.Owner;

@Path("/owner")
public interface OwnerIService {
	
	//// "localhost:8090/AppCat-Web/rest/owner/new/Erinco/Pallazo" => new owner
	@GET
	@Produces("application/json")
	@Path("/new/{name}/{surname}")
	public Owner getNewOwner(@PathParam("name") String newName, @PathParam("surname") String newSurname);
	
}
