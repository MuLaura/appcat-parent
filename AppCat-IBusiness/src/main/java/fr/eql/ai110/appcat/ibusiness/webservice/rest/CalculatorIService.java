package fr.eql.ai110.appcat.ibusiness.webservice.rest;

import javax.websocket.server.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/calc")
public interface CalculatorIService {

	// "localhost:8090/AppCat-Web/rest/calc/add/22/12" => 34
	@GET
	@Produces("text/plain")
	@Path("/add/{first}/{second}")
	public int add(@PathParam("first") int firstNumber, @PathParam("first") int secondNumber);
	
	// "localhost:8090/AppCat-Web/rest/calc/add/2/3" => 6
	@GET
	@Produces("text/plain")
	@Path("/mult/{first}/{second}")
	public int multiplier(int first, int second);
}
