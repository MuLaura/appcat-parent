package fr.eql.ai110.appcat.idao;

import java.util.List;

public interface GenericIDAO<T> {

	//méthode prend un objet générique non défini, le type est indiqué au moment de l'implémentation/extension
	T add(T t);	
	boolean delete(T t);
	T update(T t);
	T getById(int id);
	List<T> getAll();
}
