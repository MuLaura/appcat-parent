package fr.eql.ai110.appcat.idao;

import fr.eql.ai110.appcat.entity.Owner;

public interface OwnerIDAO extends GenericIDAO<Owner> {

	Owner authenticate(String login, String hashedPassword);

	Long findNbOwners();
	
	Owner add(Owner owner);
	
	Owner addOwner(Owner owner);

	Owner getOwnerByLogin(String login);
	Owner getSaltByLogin(String login);
	
}
