package fr.eql.ai110.appcat.idao;

import java.util.Set;

import fr.eql.ai110.appcat.entity.Cat;
import fr.eql.ai110.appcat.entity.Owner;

public interface CatIDAO extends GenericIDAO<Cat>{
	
	
Set<Cat> findCatsByOwner(Owner owner);

}
