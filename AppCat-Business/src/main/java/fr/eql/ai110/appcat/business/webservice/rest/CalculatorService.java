package fr.eql.ai110.appcat.business.webservice.rest;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import fr.eql.ai110.appcat.ibusiness.webservice.rest.CalculatorIService;

@Stateless
@LocalBean
public class CalculatorService implements CalculatorIService {

	@Override
	public int add(int firstNumber, int secondNumber) {
		
		return firstNumber + secondNumber;
	}

	@Override
	public int multiplier(int first, int second) {
		
		return first * second;
	}

}
