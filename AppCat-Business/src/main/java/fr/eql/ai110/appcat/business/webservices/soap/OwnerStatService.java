package fr.eql.ai110.appcat.business.webservices.soap;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;

import fr.eql.ai110.appcat.ibusiness.webservice.soap.OwnerStatIService;
import fr.eql.ai110.appcat.idao.OwnerIDAO;

@WebService(targetNamespace = "http://ai110.owner.stat.com", endpointInterface = "fr.eql.ai110.appcat.ibusiness.webservice.soap.OwnerStatIService",
 serviceName = "ownerStatService", portName = "ownerStatPort")
@Remote(OwnerStatIService.class)
@Stateless
public class OwnerStatService implements OwnerStatIService {

	private OwnerIDAO ownerDao;
	
	@Override
	public Long getNbOwners() {
		return ownerDao.findNbOwners();
	}

	
	
}
