package fr.eql.ai110.appcat.business.webservice.rest;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import fr.eql.ai110.appcat.entity.Cat;
import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.ibusiness.webservice.rest.OwnerIService;

@Stateless
@LocalBean
public class OwnerService implements OwnerIService{

	@Override
	public Owner getNewOwner(String newName, String newSurname) {
		Set<Cat> cats = new HashSet<Cat>();
		Collections.addAll(
		cats,
		new Cat(null, "Caramel", "", null, "", null, null),
		new Cat(null, "Biscuit", "", null, "", null, null),
		new Cat(null, "Praline", "", null, "", null, null)
		);
		return new Owner(null, newName, newSurname, "", "", "", true, cats);
	}

}
