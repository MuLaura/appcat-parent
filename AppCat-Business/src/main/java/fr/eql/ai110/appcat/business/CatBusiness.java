package fr.eql.ai110.appcat.business;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.appcat.entity.Cat;
import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.ibusiness.CatIBusiness;
import fr.eql.ai110.appcat.idao.CatIDAO;

@Remote(CatIBusiness.class)
@Stateless
public class CatBusiness implements CatIBusiness {
	
	@EJB
	CatIDAO catDAO;

	@Override
	public Set<Cat> getCatsByOwner(Owner owner) {
		return catDAO.findCatsByOwner(owner);
	}
	
}
