package fr.eql.ai110.appcat.business;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.ibusiness.AccountIBusiness;
import fr.eql.ai110.appcat.idao.OwnerIDAO;

@Remote(AccountIBusiness.class)
@Stateless
public class AccountBusiness implements AccountIBusiness {

	@EJB
	private OwnerIDAO ownerDAO;
	
	@Override
	public Owner connect(String login, String password) {
		Owner owner = ownerDAO.getOwnerByLogin(login);
		StringBuffer myBuffer = new StringBuffer(password);
		myBuffer.append(owner.getSalt());
		password = myBuffer.toString();
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			password = new String(encodedhash, StandardCharsets.UTF_8);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
			
		return ownerDAO.authenticate(login, password);
	}
	
//	public Owner connect(String login, String password) {
//		Owner owner = ownerDAO.getOwnerByLogin(login);
//		String salt = ownerDAO.getSaltByLogin(login);
//		
//		String hashedPassword = hashPassword(salt, password);
//			
//		return ownerDAO.authenticate(login, hashedPassword);
//	}

//	@Override
//	public void register(Owner owner) {
//		String password = owner.getPassword();
//		MessageDigest digest;
//		try {
//			digest = MessageDigest.getInstance("SHA-256");
//			byte[] encodedhash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
//			String hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
//			owner.setPassword(hashedPassword);
//		} catch (NoSuchAlgorithmException e) {
//			e.printStackTrace();
//		}
//		
//		ownerDAO.register(owner);
//	}

	@Override
	public Owner addOwner(Owner owner, String password) {
		
		String salt = generateSalt();
		owner.setSalt(salt);
		
//		StringBuffer myBuffer = new StringBuffer(password);
//		myBuffer.append(owner.getSalt());
//		password = myBuffer.toString();
		
		String hashedPassword = hashPassword(salt, password);
		owner.setPassword(hashedPassword);
		
		return ownerDAO.addOwner(owner);
	}

	private String generateSalt() {
		Random aRandom = new Random();
		int myRandom = aRandom.nextInt(999);
		String salt = Integer.toString(myRandom);
		return salt;
}

	public String hashPassword(String salt, String password) {
		String saltedPassword = password + salt;
		
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] encodedhash = digest.digest(saltedPassword.getBytes(StandardCharsets.UTF_8));
			String hashedPassword = new String(encodedhash, StandardCharsets.UTF_8);
			return hashedPassword;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return saltedPassword;
	}
	
	
}
