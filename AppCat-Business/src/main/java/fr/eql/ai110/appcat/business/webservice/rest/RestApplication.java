package fr.eql.ai110.appcat.business.webservice.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;



@ApplicationPath("/rest")
public class RestApplication extends Application {
	
	@Override
	public Set<Class<?>> getClasses() {
	Set<Class<?>> classes = new HashSet<Class<?>>();
	classes.add(CalculatorService.class);
	classes.add(OwnerService.class);
	return classes;
	}

}