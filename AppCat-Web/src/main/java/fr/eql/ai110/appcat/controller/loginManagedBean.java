package fr.eql.ai110.appcat.controller;

import java.io.Serializable;
import java.util.Random;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.ibusiness.AccountIBusiness;


@ManagedBean(name="mbLogin")
@SessionScoped
public class loginManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String login;
	private String password;
	private String name;
	private String surname;
	private Owner owner;
	
	@EJB
	private AccountIBusiness accountBusiness;
	
	public String register() {
		String forward = "/login.xhtml?faces-redirection=false";
		owner = new Owner(null, name, surname, login, null, null, false, null);
		
		accountBusiness.addOwner(owner, password);
		return forward;
	}
	
	public String connect() {
		String forward = null;
		
		owner = accountBusiness.connect(login, password);
		
		if (isConnected()) {
			forward = "/index.xhtml?faces-redirection=true";
		} else {
			FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login/Password incorrect", 
					"Incorrect login/password");
			FacesContext.getCurrentInstance().addMessage("loginForm:inLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inPassword", facesMessage);
			forward= "/login.xhtml?faces-redirection=false";
			
		}
		return forward;
	}
	
	public boolean isConnected() {
		return owner!=null;
	}
	
	public String disconnect() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		session.invalidate();
		owner = null;
		return "/login.xhtml?faces-redirection=true";
	}

	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	

}
