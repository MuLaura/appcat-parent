package fr.eql.ai110.appcat.controller;

import java.io.Serializable;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import fr.eql.ai110.appcat.entity.Cat;
import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.ibusiness.CatIBusiness;

@ManagedBean(name="mbCat")
@SessionScoped
public class catManagedBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbLogin.owner}")
	private Owner connectedOwner;
	
	private Set<Cat> connectedOwnerCats;
	
	@EJB
	private CatIBusiness catBusiness;
	
	@PostConstruct
	public void init() {
		connectedOwnerCats = catBusiness.getCatsByOwner(connectedOwner);
	}

	public Owner getConnectedOwner() {
		return connectedOwner;
	}

	public void setConnectedOwner(Owner connectedOwner) {
		this.connectedOwner = connectedOwner;
	}

	public Set<Cat> getConnectedOwnerCats() {
		return connectedOwnerCats;
	}

	public void setConnectedOwnerCats(Set<Cat> connectedOwnerCats) {
		this.connectedOwnerCats = connectedOwnerCats;
	}

	public CatIBusiness getCatBusiness() {
		return catBusiness;
	}

	public void setCatBusiness(CatIBusiness catBusiness) {
		this.catBusiness = catBusiness;
	}
	
	

}
