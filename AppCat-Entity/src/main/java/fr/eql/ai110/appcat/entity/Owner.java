package fr.eql.ai110.appcat.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="owner")
public class Owner implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "surname")
	private String surname;
	@Column(name = "login")
	private String login;
	@Column(name = "password")
	private String password;
	
	@Column(name = "salt")
	private String salt;
	@Column(name = "admin")
	private boolean isAdmin;
	// Cascade indique s'il faut faire cascader les opérations sur les objets associés et dans quelle type d'opération
	// Fetch doit faire remonter tout de suite (eager) ou éventuellement (lazy) les collections contenues dans l'objet.
	@OneToMany(mappedBy = "owner", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private Set<Cat> cats;
	
	public Owner() {
		super();
	}


	public Owner(Integer id, String name, String surname, String login, String password, String salt, boolean isAdmin,
			Set<Cat> cats) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.login = login;
		this.password = password;
		this.isAdmin = isAdmin;
		this.salt = salt;
		this.cats = cats;
	}



	public boolean isAdmin() {
		return isAdmin;
	}


	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Set<Cat> getCats() {
		return cats;
	}
	public void setCats(Set<Cat> cats) {
		this.cats = cats;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getSalt() {
		return salt;
	}


	public void setSalt(String salt) {
		this.salt = salt;
	}
	
	

}
