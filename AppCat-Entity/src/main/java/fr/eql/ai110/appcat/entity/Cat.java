package fr.eql.ai110.appcat.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="cat")
public class Cat implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "race")
	private String race;
	@Column(name = "birthdate")
	private Date birthdate;
	@Column(name = "photo")
	private String photo;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private Owner owner;
	@OneToMany(mappedBy = "cat", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Toy> toys;
	
	public Cat() {
		super();
	}
	
	public Cat(Integer id, String name, String race, Date birthdate, String photo, Owner owner, Set<Toy> toys) {
		super();
		this.id = id;
		this.name = name;
		this.race = race;
		this.birthdate = birthdate;
		this.photo = photo;
		this.owner = owner;
		this.toys = toys;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Set<Toy> getToys() {
		return toys;
	}

	public void setToys(Set<Toy> toys) {
		this.toys = toys;
	}
	
	
	
	
}
