INSERT INTO `owner` (name, surname, login, password, salt, admin) VALUES ('Axel', 'Katz', 'akatz', 'toto', '112', false);
INSERT INTO `owner` (name, surname, login, password, salt, admin) VALUES ('Sylvie', 'Ma', 'vivi', 'titi', '358', false);
INSERT INTO `owner` (name, surname, login, password, salt, admin) VALUES ('Laura', 'Murat', 'mulora', 'tata', '132', true);

INSERT INTO `cat` (name, race, birthdate, photo, owner_id) VALUES ('Ernest', 'Main Coon', '2012-12-12', 'ernest.png', 1);
INSERT INTO `cat` (name, race, birthdate, photo, owner_id) VALUES ('Miso', 'Chat roux', '2012-12-12', 'garfield.jpg', 2);
INSERT INTO `cat` (name, race, birthdate, photo, owner_id) VALUES ('Decibelle', 'Bleu Russe', '2015-08-12', 'flippette.jpg', 3);

INSERT INTO `toy` (name, cat_id) VALUES ('Canard', 1);
INSERT INTO `toy` (name, cat_id) VALUES ('Souris', 2);
INSERT INTO `toy` (name, cat_id) VALUES ('Pelote de laine', 3);