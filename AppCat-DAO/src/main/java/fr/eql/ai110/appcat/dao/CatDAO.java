package fr.eql.ai110.appcat.dao;

import java.util.HashSet;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.appcat.entity.Cat;
import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.idao.CatIDAO;
import fr.eql.ai110.appcat.idao.OwnerIDAO;

@Remote(CatIDAO.class)
@Stateless
public class CatDAO extends GenericDAO<Cat> implements CatIDAO {

	@Override
	public Set<Cat> findCatsByOwner(Owner owner) {
		Query query = em.createQuery("SELECT c FROM Cat c WHERE c.owner = :ownerParam");
		query.setParameter("ownerParam", owner);
		return new HashSet<Cat>(query.getResultList());
	}

}
