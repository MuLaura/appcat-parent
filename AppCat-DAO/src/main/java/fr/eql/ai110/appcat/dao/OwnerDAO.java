package fr.eql.ai110.appcat.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.appcat.entity.Owner;
import fr.eql.ai110.appcat.idao.OwnerIDAO;

@Remote(OwnerIDAO.class)
@Stateless
public class OwnerDAO extends GenericDAO<Owner> implements OwnerIDAO{

	@Override
	public Owner authenticate(String login, String hashedPassword) {
		Owner owner = null;
		List<Owner> owners;
		
		Query query = em.createQuery("SELECT o FROM Owner o WHERE o.login = :loginParam AND o.password=:hashedPasswordParam");
		query.setParameter("loginParam", login);
		query.setParameter("hashedPasswordParam", hashedPassword);
		owners = query.getResultList();
		if (owners.size() > 0) {
			owner = owners.get(0);
		}
		
		return owner;
	}

	@Override
	public Owner getOwnerByLogin(String login) {
		Owner owner = null;
		List<Owner> owners;
		
		Query query = em.createQuery("SELECT o FROM Owner o WHERE o.login = :loginParam");
		query.setParameter("loginParam", login);
		owners = query.getResultList();
		if (owners.size() > 0) {
			owner = owners.get(0);
		}
		
		return owner;
	}
	
	@Override
	public Long findNbOwners() {
		Query query = em.createQuery("SELECT COUNT(o) FROM Owner o");
		return (Long) query.getSingleResult();
	}

	//Method-1 : not generic, not scalable  and prone to errors
    //WARNING JPA DOES NOT SUPPORT INSERT STATEMENTS (use NATIVE QUERY)
//    @Override
//    public Owner add(Owner owner) {
//        
//        Query query = em.createNativeQuery("INSERT INTO Owner (login, password, name, surname, salt, admin) "
//                + "VALUES (:login, :password, :name, :surname, :salt, :admin)");
//        
//        query.setParameter("login", owner.getLogin());
//        query.setParameter("password", owner.getPassword());
//        query.setParameter("name", owner.getName());
//        query.setParameter("surname", owner.getSurname());
//        query.setParameter("salt", owner.getSalt());
//        query.setParameter("admin", owner.getisAdmin());
//        query.executeUpdate();
//        
//        return owner;
//        
//    }    
    
    //Method-2 : using generic statement (has to exist), scalable happy :)
	public Owner addOwner(Owner owner) {
		em.persist(owner);
		return owner;
	}

	@Override
	public Owner getSaltByLogin(String login) {
		Query query = em.createQuery("SELECT o.salt FROM Owner o WHERE o.login = :loginParam");
		query.setParameter("loginParam", login);
		return null;
	}
	
}
